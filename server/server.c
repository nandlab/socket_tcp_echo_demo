/*
 ============================================================================
 Name        : socket_tcp_echo_server.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT 8080

ssize_t writeall(int fd, const void *buf, size_t n) {
	ssize_t res;
	ssize_t nwrote;
	for (nwrote = 0; nwrote < n; nwrote += res) {
		res = write(fd, buf + nwrote, n - nwrote);
		if (res < 0) { return -1; }
	}
	return n;
}

int main(void) {
	struct sockaddr_in serv_addr = {
		.sin_family = AF_INET,
		.sin_port = htons(PORT),
		.sin_addr = (struct in_addr) {.s_addr = htonl(INADDR_ANY)}
	};
	int server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd == -1) {
		puts("Could not create socket!");
		exit(EXIT_FAILURE);
	}
	puts("Socket created successfully.");
	if (bind(server_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		printf("Failed to bind to port %d!\n", PORT);
		exit(EXIT_FAILURE);
	}
	printf("Bound to port %d.\n", PORT);
	if (listen(server_fd, 10)) {
		puts("Cannot listen!");
		exit(EXIT_FAILURE);
	}
	puts("Listening...");
	while (1) {
		struct sockaddr_in client_addr;
		int connsock_fd;
		socklen_t clilen = sizeof(client_addr);
		puts("\nWaiting for incoming connection...");
		connsock_fd = accept(server_fd, (struct sockaddr *) &client_addr, &clilen);
		if (connsock_fd < 0) {
			puts("Error on accept!");
			exit(EXIT_FAILURE);
		}
		puts("Connection accepted.");
		char msg[1048576] = {};
		const size_t MSGSIZE = sizeof(msg)/sizeof(*msg);
		while (1) {
			ssize_t nrcv = read(connsock_fd, msg, MSGSIZE);
			if (nrcv < 0) {
				puts("Failed to read data from connected client!");
				exit(EXIT_FAILURE);
			}
			else if (nrcv == 0) {
				puts("Client disconnected.");
				break;
			}
			else { // n > 0
				printf("Received message is %lu bytes long\n", nrcv);
				ssize_t nsent = writeall(connsock_fd, msg, nrcv);
				if (nsent < 0) {
					puts("Failed to echo back. Perhaps the client already disconnected.");
					break;
				}
				else {
					printf("%lu characters echoed back.\n", nsent);
				}
			}
		}
		close(connsock_fd);
		puts("Connection closed.");
	}
	close(server_fd);
	puts("Port closed.");
	return EXIT_SUCCESS;
}
